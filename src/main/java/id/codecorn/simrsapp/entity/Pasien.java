package id.codecorn.simrsapp.entity;


import lombok.Data;
import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;

@Data @Entity @Table(name = "m_pasien")
@SQLDelete(sql = "UPDATE m_pasien SET status_record = 'INACTIVE' WHERE id=?")
@Where(clause = "status_record='ACTIVE'")
public class Pasien extends BaseEntity {

    @NotNull @Column(length = 20)
    private String norm;

    @NotNull @Column(length = 150)
    private String nama;

    @Column(name = "gelar_depan",length = 10)
    private String gelarDepan;

    @Column(name = "gelar_belakang",length = 10)
    private String gelaBelakang;

    @NotNull @Column(length = 3)
    private Integer kelamin = 1;

    @Column(name = "tempat_lahir",length = 100)
    private String tempatLahir;

    @Column(name = "tanggal_lahir")
    private LocalDate tanggaLahir;

    @Enumerated(EnumType.STRING)
    private Agama agama = Agama.ISLAM;

    @OneToOne @JoinColumn(name = "id_alamat")
    private Alamat alamat;

    @Column(length = 150)
    private String pekerjaan;

    @Column(name = "status_perkawinan")
    private Integer statusPerkawinan = 1;

    @Column(length = 5)
    private String golonganDarah;
}
