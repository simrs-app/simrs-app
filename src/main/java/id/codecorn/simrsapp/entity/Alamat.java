package id.codecorn.simrsapp.entity;


import lombok.Data;

import javax.persistence.*;

@Data @Entity @Table(name = "t_alamat")
public class Alamat {

    @Id @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(length = 150)
    private String alamat;

    @Column(length = 5)
    private String rt;

    @Column(length = 5)
    private String rw;

    @Column(length = 8)
    private String kodepos;
}
