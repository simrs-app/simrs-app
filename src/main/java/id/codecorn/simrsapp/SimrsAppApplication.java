package id.codecorn.simrsapp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SimrsAppApplication {

	public static void main(String[] args) {
		SpringApplication.run(SimrsAppApplication.class, args);
	}

}
