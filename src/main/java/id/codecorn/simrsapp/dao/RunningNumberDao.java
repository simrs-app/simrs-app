package id.codecorn.simrsapp.dao;

import id.codecorn.simrsapp.entity.RunningNumber;
import org.springframework.data.repository.CrudRepository;

public interface RunningNumberDao extends CrudRepository<RunningNumber, String> {
    RunningNumber findByPrefix(String prefix);
}
