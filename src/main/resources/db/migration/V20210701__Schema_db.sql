create table m_pasien(
    id varchar(255) not null,
    norm varchar(20) not null,
    nama varchar(150) not null,
    gelar_depan varchar(10),
    gelar_belakang varchar(10),
    kelamin integer not null,
    tempat_lahir varchar(100),
    tanggal_lahir date,
    pekerjaan varchar(150),
    agama varchar(255),
    status_perkawinan integer,
    golongan_darah varchar(5),
    id_alamat integer,
    created_at datetime(6),
    created_by varchar(255),
    status_record varchar(255),
    updated_at datetime(6),
    updated_by varchar(255),
    primary key (id)
) engine=InnoDB;

create table t_alamat(
    id integer not null auto_increment,
    alamat varchar(150),
    rt varchar(5),
    rw varchar(5),
    kodepos varchar(8),
    primary key (id)
) engine=InnoDB;

alter table m_pasien add constraint FKpjgrycesc0i92fnaxhcloyqat foreign key (id_alamat) references t_alamat (id);


