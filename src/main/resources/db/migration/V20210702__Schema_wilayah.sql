CREATE TABLE m_kabupaten(
  id int NOT NULL,
  nama varchar(255) NOT NULL,
  id_prov int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;


CREATE TABLE m_kecamatan (
  id int NOT NULL,
  nama varchar(255) NOT NULL,
  id_kab int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE m_kelurahan (
  id bigint NOT NULL,
  nama varchar(255) NOT NULL,
  id_kec int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE m_provinsi (
  id int NOT NULL,
  nama varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table m_kabupaten
--
ALTER TABLE m_kabupaten
  ADD PRIMARY KEY (id),
  ADD KEY id_prov (id_prov);

--
-- Indexes for table m_kecamatan
--
ALTER TABLE m_kecamatan
  ADD PRIMARY KEY (id),
  ADD KEY id_kab (id_kab);

--
-- Indexes for table m_kelurahan
--
ALTER TABLE m_kelurahan
  ADD PRIMARY KEY (id),
  ADD KEY id_kec (id_kec);

--
-- Indexes for table m_provinsi
--
ALTER TABLE m_provinsi
  ADD PRIMARY KEY (id);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table m_kabupaten
--
ALTER TABLE m_kabupaten
  MODIFY id int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9472;

--
-- AUTO_INCREMENT for table m_kecamatan
--
ALTER TABLE m_kecamatan
  MODIFY id int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9471041;

--
-- AUTO_INCREMENT for table m_kelurahan
--
ALTER TABLE m_kelurahan
  MODIFY id bigint NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9471040009;

--
-- AUTO_INCREMENT for table m_provinsi
--
ALTER TABLE m_provinsi
  MODIFY id int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=95;

--
-- Constraints for dumped tables
--

--
-- Constraints for table m_kabupaten
--
ALTER TABLE m_kabupaten
  ADD CONSTRAINT m_kabupaten_ibfk_1 FOREIGN KEY (id_prov) REFERENCES m_provinsi (id) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table m_kecamatan
--
ALTER TABLE m_kecamatan
  ADD CONSTRAINT m_kecamatan_ibfk_1 FOREIGN KEY (id_kab) REFERENCES m_kabupaten (id) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table m_kelurahan
--
ALTER TABLE m_kelurahan
  ADD CONSTRAINT m_kelurahan_ibfk_1 FOREIGN KEY (id_kec) REFERENCES m_kecamatan (id) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;